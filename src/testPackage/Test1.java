package testPackage;

import org.openqa.selenium.By; 
import org.openqa.selenium.WebDriver; 
import org.openqa.selenium.chrome.ChromeDriver;

public class Test1 {
	public static void main(String[] args) throws InterruptedException
	{
	// 1. Launch the browser 
	System.setProperty("webdriver.chrome.driver", "D:\\Software\\chromedriver_win32\\chromedriver.exe");
	WebDriver dr = new ChromeDriver(); 
	//2. Open the application using https://petstore.octoperf.com/ 
	dr.get("https://petstore.octoperf.com/"); 
	//3. Click on the link Enter the store 
	dr.findElement(By.linkText("Enter the Store")).click();
	//4. Click on Sign in link 
	dr.findElement(By.linkText("Sign In")).click(); 
	//5. Enter the username 
	dr.findElement(By.name("username")).sendKeys("j2ee"); 
	dr.findElement(By.name("password")).clear(); 
	//6. Enter the password 
	dr.findElement(By.name("password")).sendKeys("j2ee"); 
	//7. clink on sing in 
	dr.findElement(By.name("signon")).click(); 
	//8. Click on Sign off 
	dr.findElement(By.linkText("Sign Out")).click();
	}
}
